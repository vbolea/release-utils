#!/usr/bin/env python3
'''
Backport merge requests for a release branch

This script creates a topic which finds merge requests which have been added to
a milestone, but are missing from a given integration branch. A branch will be
created which backports the relevant merge requests for the target branch.

For each of these MRs, check if it has been merged into the project's branch
tracking the milestone. For each that has not, attempt to bring it into the
tracking branch, merging the original code if possible, or cherry-picking those
that are not. Note that cherry-pick'ing branches that contain merges or end up
conflicting cause the entire MR in question to be skipped.

At the end, a report of the MRs found and acted on are listed.

The only required flags are:

  - `-t`: the GitLab API token
  - `-m`: the milestone to query for merge requests
  - `-p`: the name of the project

Optional flags include:

  - `-g`: the GitLab instance to communicate with (defaults to
    `gitlab.kitware.com`)
  - `-q`: the branch which contains MRs which may need backported (defaults to
    `master`)
  - `-b`: the branch which tracks the release which tracks the milestone
    (defaults to `release`)
  - `-r`: the name of the remote to query (defaults to `origin`)
  - `-n`: the name of the branch to create (defaults to
    `backport-missed-mrs-{milestone}`)
'''

import argparse
import git
import requests
import shutil
import urllib


try:
    from termcolor import colored
except ImportError:
    def colored(text, color):
        return text


try:
    from progress.bar import Bar
except ImportError:
    class Bar(object):
        def __init__(self, *args, **kwargs):
            pass

        def next(self):
            pass

        def iter(self, i):
            return i

        def finish(self):
            pass


def options():
    p = argparse.ArgumentParser(
        description='Gather merge requests which need to be backported')
    p.add_argument('-g', '--gitlab', type=str, default='gitlab.kitware.com',
                   help='the gitlab instance to communicate with')
    p.add_argument('-t', '--token', type=str, required=True,
                   help='gitlab api token (gitlab->settings->access tokens)')
    p.add_argument('-m', '--milestone', type=str, required=True,
                   help='milestone to query for merge requests')
    p.add_argument('-p', '--project', type=str, required=True,
                   help='the name of the project')
    p.add_argument('-r', '--remote', type=str, default='origin',
                   help='the name of the remote')
    p.add_argument('-b', '--branch', type=str, default='release',
                   help='the integration branch for the target release')
    p.add_argument('-q', '--query', type=str, default='master',
                   help='the main branch for the project')
    p.add_argument('-n', '--name', type=str,
                   help='the name of the branch to create')
    return p


class Gitlab(object):
    def __init__(self, gitlab_url, token):
        self.gitlab_url = f'https://{gitlab_url}/api/v4'
        self.headers = {
            'PRIVATE-TOKEN': token,
        }

    def get(self, endpoint, **kwargs):
        rsp = requests.get(f'{self.gitlab_url}/{endpoint}',
                           headers=self.headers,
                           params=kwargs)

        if rsp.status_code != 200:
            raise RuntimeError(f'Failed request to {endpoint}: {rsp.content}')

        return rsp.json()

    def get_paged(self, path, **kwargs):
        kwargs.update({
            'page': 1,
            'per_page': 100,
        })

        full = []
        while True:
            items = self.get(path, **kwargs)
            if not items:
                break

            full += items
            if len(items) < kwargs['per_page']:
                break
            kwargs['page'] += 1

        return full

    def post(self, endpoint, **kwargs):
        rsp = requests.post(f'{self.gitlab_url}/{endpoint}',
                            headers=self.headers,
                            params=kwargs)

        if rsp.status_code != 201:
            raise RuntimeError(f'Failed post to {endpoint}: {rsp.content}')

        return rsp.json()


def find_merged_mrs(repo, opts):
    # Gather the list of merged MRs.
    MERGE_REQUEST_TRAILER_PREFIX = 'Merge-request: !'
    merged_mr_ids = set()
    branch_point = None

    bar = Bar('Searching for merged merge requests... %(hexsha)s')
    bar.hexsha = '0000000000000000000000000000000000000000'
    parents = repo.git.rev_list(
        '--first-parent',
        '--min-parents=2',
        f'{opts.remote}/{opts.branch}',
    )

    for sha in bar.iter(parents.split('\n')):
        sha = sha.rstrip()
        if not sha:
            continue
        commit = repo.commit(sha)
        bar.hexsha = commit.hexsha
        # See if we're still tracking merges into the release branch.
        if not commit.summary.endswith(f'into {opts.branch}') and \
           branch_point is None:
            branch_point = commit.hexsha
        for line in commit.message.split('\n'):
            if line.startswith(MERGE_REQUEST_TRAILER_PREFIX):
                mr_id = int(line[len(MERGE_REQUEST_TRAILER_PREFIX):])
                merged_mr_ids.add(mr_id)

    return {
        'mr_ids': merged_mr_ids,
        'branch_point': branch_point,
    }


def find_merged_mr_commits(repo, opts):
    bar = Bar('Searching for the first ineligible commit... %(hexsha)s')
    bar.hexsha = '0000000000000000000000000000000000000000'
    parents = repo.git.rev_list(
        '--first-parent',
        f'{opts.remote}/{opts.query}',
    )
    merge_commits = set()

    # Find the first ineligible commit in the release branch.
    ineligible_commit = None
    for sha in bar.iter(parents.split('\n')):
        sha = sha.rstrip()
        if not sha:
            continue
        if sha == branch_point:
            break
        bar.hexsha = sha
        merge_commits.add(sha)
        ineligible_commit = sha

    return {
        'merge_commits': merge_commits,
        'ineligible_commit': ineligible_commit,
    }


def backport_mrs(repo, opts, mrs, master_commits):
    bar = Bar('Scanning merge requests... !%(iid)d')
    bar.iid = 0

    for mr in bar.iter(mrs):
        bar.iid = mr['iid']

        # This MR has already landed.
        if mr['iid'] in merged_mr_ids:
            continue

        # If this MR can be merged, use it as-is.
        needs_cherry_pick = repo.is_ancestor(ineligible_commit, mr['sha'])
        manual = None

        if needs_cherry_pick:
            # Create a branch to cherry-pick onto.
            backport_head = repo.create_head(
                f'backport-mr-{mr["iid"]}-for-{opts.milestone}',
                f'{opts.remote}/{opts.branch}',
                force=True,
            )
            backport_head.checkout(force=True)

            commits = []
            for commit in repo.iter_commits(f'{opts.remote}/{opts.branch}..{mr["sha"]}'):
                # We found this MR's branch point from master. Stop collecting
                # commits from the branch. Note that we are assuming merge
                # requests to not merge master back into themselves to resolve
                # conflicts; we are blind before that point.
                if commit.hexsha in master_commits:
                    break
                commits.append(commit)

            commits.reverse()
            for commit in commits:
                if not commit.parents:
                    manual = 'root commit found'
                    break
                if len(commit.parents) > 1:
                    manual = 'merge commit found'
                    break
                try:
                    repo.git.cherry_pick(commit.hexsha, x=True)
                except git.GitCommandError as e:
                    errstr = str(e)
                    if 'git commit --allow-empty' in errstr:
                        repo.git.cherry_pick('--abort')
                    elif 'CONFLICT' in errstr:
                        manual = 'cherry-pick failed (conflict)'
                        break
                    else:
                        manual = f'cherry-pick failed ({e})'
                        break

            working_branch.checkout(force=True)
            if manual:
                repo.delete_head(backport_head, force=True)
        else:
            backport_head = repo.create_head(
                f'{mr["source_branch"]}-for-{opts.milestone}',
                mr['sha'],
                force=True,
            )

        if manual is not None:
            manual_mrs.append((mr, manual))
        else:
            merge_base = repo.merge_base(backport_head, working_branch)
            try:
                repo.index.merge_tree(backport_head, base=merge_base)
                repo.index.commit(
                    f'''Merge topic '{mr["source_branch"]}' into {opts.name}

Merge-request: !{mr["iid"]}
''',
                    parent_commits=(
                        working_branch.commit,
                        backport_head.commit,
                    ))

                old_parent = repo.commit(f'{opts.name}~')
                diff_index = repo.index.diff(old_parent)
                if len(diff_index) == 0:
                    repo.git.reset(old_parent.hexsha)
                    skipped_mrs.append(mr)
                else:
                    merged_mrs.append(mr)
            except git.GitCommandError as e:
                manual_mrs.append((mr, e))

    return {
        'manual': manual_mrs,
        'merged': merged_mrs,
        'skipped': skipped_mrs,
    }


if __name__ == '__main__':
    p = options()
    opts = p.parse_args()

    # Make the default branch name.
    if opts.name is None:
        opts.name = f'backport-missed-mrs-{opts.milestone}'

    # Ensure the project name is URL-safe.
    if '%' not in opts.project:
        opts.project = urllib.parse.quote(opts.project, safe='')

    gitlab = Gitlab(opts.gitlab, opts.token)

    main_repo = git.repo('.')

    # Update the main repository.
    main_repo.fetch(opts.remote)

    mr_info = find_merged_mrs(main_repo, opts)
    merged_mr_ids = mr_info['mr_ids']
    branch_point = mr_info['branch_point']

    merge_information = find_merged_mr_commits(main_repo, opts)
    merge_commits = merge_information['merge_commits']
    ineligible_commit = merge_information['ineligible_commit']

    # Grab the merged mrs with the milestone of interest.
    merged_requests = gitlab.get_paged(
        'projects/{opts.project}/merge_requests',
        milestone=opts.milestone,
        sort='asc',
        state='merged',
    )

    try:
        shutil.rmtree('workdir')
    except Exception:
        pass
    main_repo.git.worktree(
        'add',
        '-f',
        'workdir',
        f'{opts.remote}/{opts.branch}',
    )
    repo = git.Repo('workdir')

    # Create a branch based off of release for performing the backports
    working_branch = repo.create_head(
        opts.name,
        commit=f'{opts.remote}/{opts.branch}',
        force=True,
    )
    working_branch.checkout()

    mr_results = backport_mrs(repo, opts, merged_requests, merge_commits)
    manual_mrs = mr_results['manual']
    merged_mrs = mr_results['merged']
    skipped_mrs = mr_results['skipped']

    if skipped_mrs:
        colored(
            'Merge requests which appear to have already been backported:',
            'yellow',
        )
        for mr in skipped_mrs:
            colored(f'    !{mr["iid"]} {mr["web_url"]}', 'green')
        print()
    if merged_mrs:
        colored('Merge requests which have been backported:', 'yellow')
        for mr in merged_mrs:
            colored(f'    !{mr["iid"]} {mr["web_url"]}', 'green')
            colored(f'        {mr["title"]}', 'green')
            print()
    if manual_mrs:
        colored('Merge requests which must be backported manually:', 'yellow')
        for (mr, reason) in manual_mrs:
            colored(f'    !{mr["iid"]} {mr["web_url"]}', 'green')
            colored(f'        {mr["title"]}', 'green')
            colored(f'        {reason}', 'green')
            print()
